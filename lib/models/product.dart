class Product {
  final int price;
  final String id, titile, category, image, subTitle, description;

  var title;
  Product(this.titile, {
    required this.id,
    required this.price,
    this.title,
    required this.category,
    required this.image,
    required this.subTitle,
    required this.description,
  });
  //create category
  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
      id: json["id"],
      title: json["title"],
      image: json["image"],
      price: json["price"],
      category: json["category"],
      subTitle: json["subTitle"],
      description: json["description"],
    );
  }

  fromJson(data) {}
}

Product product = Product(
  id: "1",
  price: 16000,
  title: "wood frame",
  image: "assets/images/image_1.png",
  category: "chair",
  subTitle: "Tieon armchair",
  description: description,
);
String description =
    "this armchair is an elegant and functional piece of furniture";
